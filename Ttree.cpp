{

#define _USE_MATH_DEFINES
#include <iostream>
#include <fstream>
using namespace std;


double _s1_MeV, _s1_vertx, _s1_verty, _s1_vertz, _s2_MeV, _s2_vertx, _s2_verty, _s2_vertz, _s1_P, _s2_P;
double wind_P, wind_p1, wind_x, wind_y, wind_z, theta_e, cos_e, theta_n, cos_n;
int count_t = 0;


/* 
//using by TChain method with directly, pro -> handling multiple file
TChain *tree1 = new TChain("ntp");		//name of it want to make tree
tree1->Add("data.root");		// RENO data file
tree1->Print();	//checking name of branch and var on the branch
//tree1 -> Add("./A/fn.root");
//tree1 -> Add("./B/1/fn_near.root");
//tree1 -> Add("./B/2/fn_front.root");
tree1->SetBranchAddress("_s1_MeV",&_s1_MeV);
tree1->SetBranchAddress("_s2_MeV",&_s2_MeV);
tree1->SetBranchAddress("_s1_vertx",&_s1_vertx);
tree1->SetBranchAddress("_s1_verty",&_s1_verty);
tree1->SetBranchAddress("_s1_vertz",&_s1_vertz);
tree1->SetBranchAddress("_s2_vertx",&_s2_vertx);
tree1->SetBranchAddress("_s2_verty",&_s2_verty);
tree1->SetBranchAddress("_s2_vertz",&_s2_vertz);
//tree1 -> LoadTree(0);	//use for-loop
*/

/*
//using by TFile,Ttree,TTreeReader with nulptr 
TFile *filein = new TFile("./B/1/fn_near.root");
TTree *tree1 = nullptr;
TTreeReader Treader("T",&filein);
TTreeReaderValue<ini> variable(Treader,"_s1_MeV");

*/

//using by TFile,Ttree without nullptr
TFile *file1 = new TFile("./B/1/data.root");
TTree *tree1 = (TTree*)file1 -> Get("ntp");	// "ntp" name of tree at ntuple in RENO data
//TNtuple *tree1 = (TNtuple*)file1 -> Get("ntp");	// "ntp" name of tree at ntuple in RENO data
//TNtuple is simple 1 Dim. data structure of TTree and old syntax...
tree1->LoadTree(0);
tree1->SetBranchAddress("_s1_MeV",&_s1_MeV);
tree1->SetBranchAddress("_s2_MeV",&_s2_MeV);
tree1->SetBranchAddress("_s1_vertx",&_s1_vertx);
tree1->SetBranchAddress("_s1_verty",&_s1_verty);
tree1->SetBranchAddress("_s1_vertz",&_s1_vertz);
tree1->SetBranchAddress("_s2_vertx",&_s2_vertx);
tree1->SetBranchAddress("_s2_verty",&_s2_verty);
tree1->SetBranchAddress("_s2_vertz",&_s2_vertz);

TFile *file2 = new TFile("result.root","recreate");
TTree *tree2 = new TTree("direction","from_data");
tree2-> Branch("X", &w4, "X/D");
tree2-> Branch("P", &wind_p1, "P/D");
tree2-> Branch("theta_e", &theta_e, "theta_e/D");
tree2-> Branch("cos_e", &cos_e, "cos_e/D");
tree2-> Branch("theta_n", &theta_n, "theta_n/D");
tree2-> Branch("theta_N", &theta_w, "theta_N/D");
tree2-> Branch("cos_n", &cos_n, "con_n/D");
tree2-> Branch("cos_N", &cos_w, "con_N/D");

int count_t_max = tree1->GetEntries();

/*
// It is possible to use the syntax of FOR LOOP based on range  ,where the syntax of modern cpp ( lasted ver. containning to c++11) 

//you want to syntax of for loop for like python syntax
//1. user-defined method of class
//2. recom. to be using boost lib.
//for(int i : boost ::irange(start,end,step)){}
*/

int t;
double w1, w2, w3, w4, cos_w, theta_w;	// average of wind vector
for (int t = 0 ; t < count_t_max; ++t)
{
	tree1->GetEntry(t);
	w1 += -_s1_vertx + _s2_vertx;
	w2 += -_s1_verty + _s2_verty;
	w3 += -_s1_vertz + _s2_vertz;
}

w1 /= count_t_max;
w2 /= count_t_max;
w2 /= count_t_max;	
w4 = sqrt(pow(w1,2)+pow(w2,2)+pow(w3,2));

for(int count_t = 0; count_t < tree1->GetEntries() ; ++count_t)
{
	tree1->GetEntry(count_t);
	wind_x = -_s1_vertx + _s2_vertx;
	wind_y = -_s1_verty + _s2_verty;
	wind_z = -_s1_vertz + _s2_vertz;
	wind_P = sqrt(pow(wind_x,2)+pow(wind_y,2)+pow(wind_z,2));
	wind_p1 = wind_P/100;
	_s1_P = sqrt(pow(_s1_vertx,2)+pow(_s1_verty,2)+pow(_s1_vertz,2));
	_s2_P = sqrt(pow(_s1_vertx,2)+pow(_s1_verty,2)+pow(_s1_vertz,2));
	cos_e = ( wind_x*_s1_vertx + wind_y*_s1_verty + wind_z*_s1_vertz)/(wind_P*_s1_P);
	//cos_n = ( wind_x*_s2_vertx + wind_y*_s2_verty + wind_z*_s2_vertz)/(wind_P*_s2_P);
	//cos_n = ( w1*_s2_vertx + w2*_s2_verty + w3*_s2_vertz)/(w4*_s2_P);
	cos_n = wind_x/wind_P;
	cos_w = wind_x/w4;
	theta_e = (180/M_PI)*acos(cos_e);
	theta_n = (180/M_PI)*acos(cos_n);
	theta_w = (180/M_PI)*acos(cos_w);

	tree2->Fill();
}

tree2->Write();
file2->Close();
file1->Close();

//Draw histrogram
TCanvas *c = new TCanvas("c","canvas",800,1000);
gStyle -> SetOptStat(0);
TFile *Drawfile = new TFile("result.root");
TTree *neutrino = (TTree*) Drawfile->Get("direction");
TTree *positron = (TTree*) Drawfile->Get("direction");
TTree *neutron = (TTree*) Drawfile->Get("direction");
TTree *positron2 = (TTree*) Drawfile->Get("direction");
TTree *neutron2 = (TTree*) Drawfile->Get("direction");

c->Divide(4,2,0,0);

c->cd(1);
//neutrino->Draw("theta_N");
neutrino->Draw("cos_n","","P E0 ][");
gPad->SetLeftMargin(0.15);
gPad->SetBottomMargin(0.15);

c->cd(2);
positron->Draw("theta_n");
gPad->SetLeftMargin(0.15);
gPad->SetBottomMargin(0.15);

c->cd(3);
neutrino->Draw("cos_n","","P*");
gPad->SetLeftMargin(0.15);
gPad->SetBottomMargin(0.15);

c->cd(4);
positron->Draw("cos_N");
gPad->SetLeftMargin(0.15);
gPad->SetBottomMargin(0.15);

c->cd(5);
positron->Draw("cos_N:P","","col");
gPad->SetLeftMargin(0.15);
gPad->SetBottomMargin(0.15);

c->cd(6);
neutron->Draw("cos_n:P","","col");
gPad->SetLeftMargin(0.15);
gPad->SetBottomMargin(0.15);

c->cd(7);
TProfile *hprof1 = new TProfile("hprof1","<cos_N>vsP[MeV]",21,0.0,20);
positron2->Draw("cos_N:P >> hprof1","abs(cos_e)<=1","");
hprof1->Draw();
gPad->SetLeftMargin(0.15);
gPad->SetBottomMargin(0.15);

c->cd(8);
TProfile *hprof2 = new TProfile("hprof2","<cos_n>vsP[MeV]",21,0.0,20);
neutron2->Draw("cos_n:P >> hprof2","abs(cos_n)<=1 " ,"");
hprof2->Draw();
gPad->SetLeftMargin(0.15);
gPad->SetBottomMargin(0.15);

}
